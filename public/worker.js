var run = 0;
var live = 0;
var data  = [];
var label = [];
for (var i = 0; i <= 200; i++) {
    label.push(i);
    data.push(0);
}

onmessage = function(e) {
    //Make sure function type is 5 for reset run/live toggles
    if(e.data[0] == 5) {
        run = e.data[1];
        live = e.data[2];
    }
    if(run) measure_time(0);
};


// Function from paper, slightly changed for interface (see "run" variable)
function measure_time(id) {
    if(run) setTimeout(measure_time, 0, id+1);
    var counter = 0;
    var begin = performance.now();
    while(performance.now() - begin < 5) {
        counter++;
    }
    publish(id, counter);
}

// In Worker method, publish method posts Data to main script
function publish(id, counter) {
    //Use lists as queues, amount of values specified above - counter goes to "data", id goes to "label"
    data.shift();
    label.shift();
    data.push(counter);
    label.push(id);

    if(run && live || !run) {
        postMessage([6, data, label]);
    }
}

